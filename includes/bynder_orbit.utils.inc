<?php

/**
 * @file
 * Utility functions for the Bynder Orbit module.
 */

use Orbit\Api\Queries\AssetPermanentPreviewQuery;

/**
 * Helper function to return the asset preview URL.
 *
 * If the preview is not in the right format we do a call to Orbit API and
 * generate it.
 *
 * @param $filename
 *    The image filename.
 * @param $orbit_id
 *    Orbit asset ID.
 * @param $image_style
 *    Image style name, to match the style stored in database.
 *
 * @return string
 *    The image preview URL, or a default `no image found`.
 *
 * @throws \Exception
 * @throws \InvalidMergeQueryException
 */
function bynder_orbit_embed_url($filename, $orbit_id, $image_style = NULL) {
  $bynder_entity = db_select('bynder_orbit_media_entity', 'b')
    ->fields('b')
    ->condition('b.bynder_orbit_id', $orbit_id, '=')
    ->execute()
    ->fetchAll();

  if (!empty($bynder_entity)) {
    $bynder_styles = unserialize(variable_get('bynder_orbit_previews'));
    $image_previews = unserialize($bynder_entity[0]->previews);
    $style_name = str_replace('bynder_orbit_', '', $image_style);
    // Process custom Bynder previews.
    if (in_array($style_name, $bynder_styles)) {
      if (isset($image_previews[$style_name])) {
        $preview_url = $image_previews[$style_name]['url'];
        // The first time we get the image and we haven't generated the
        // permanent preview the URL won't have the CDN format.
        if (!bynder_orbit_is_cdn_format($preview_url)) {
          $bynder_orbit_id = $bynder_entity[0]->bynder_orbit_id;
          $preview_url = BynderOrbitMediaApi::createOrbitClient()->getThumbnailUrl(
            AssetPermanentPreviewQuery::create($bynder_orbit_id)
              ->setWidth($image_previews[$style_name]['width'])
              ->setHeight($image_previews[$style_name]['height'])
          );
          if (isset($preview_url['url'])) {
            $preview_url = $preview_url['url'];
            $image_previews[$style_name]['url'] = $preview_url;
            // Save CDN URL to database entry.
            db_merge('bynder_orbit_media_entity')
              ->fields([
                'previews' => serialize($image_previews),
              ])
              ->condition('bynder_orbit_id', $bynder_orbit_id, '=')
              ->execute();
          }
        }
      }
    }
  }
  return bynder_orbit_is_valid_preview($preview_url) ? $preview_url : bynder_orbit_get_not_found_preview($filename);
}

/**
 * Checks if an URL is in the desired CDN format.
 *
 * @param $url
 *    The asset URL.
 *
 * @return bool
 *    Whether it matches the CDN format or not.
 */
function bynder_orbit_is_cdn_format($url) {
    return preg_match("/.*?(\\/cdn.bynder.com\\/).*/", $url);
}

/**
 * Checks if the preview URL is either CDN or just a well formed URL.
 *
 * @param $preview_url
 *
 * @return bool
 */
function bynder_orbit_is_valid_preview($preview_url) {
  $cdn_check = bynder_orbit_is_cdn_format($preview_url);
  $url_check = preg_match("/(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/", $preview_url);

  if($cdn_check || $url_check) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Helper function to return an "Image not found" and display a message.
 *
 * @param bool $filename
 *  If the filename is passed we display the warning message.
 *
 * @return string
 *  The image URL.
 */
function bynder_orbit_get_not_found_preview($filename = FALSE) {

  if($filename && !user_is_anonymous()) {
    drupal_set_message(
      t('No valid image style found for image @filename, please check your error logs for more information.',
        ['@filename' => $filename,]), 'warning');
  }

  // If everything else fails we default to image not found.
  return $GLOBALS['base_url'] . '/' . drupal_get_path('module',
      'bynder_orbit') . '/assets/no-image.png';
}
