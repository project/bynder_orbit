<?php

/**
 * @file
 * The Bynder Orbit API service class.
 */

use GuzzleHttp\Exception\RequestException;
use Orbit\Configuration\Configuration;
use Orbit\OrbitClient;

/**
 * Class serving as a service to communicate with the Orbit API.
 */
class BynderOrbitMediaApi {

  const DEFAULT_ORBIT_API_URL = "https://api.bynder.com";

  private $orbitClient;

  /**
   * BynderOrbitMediaApi constructor.
   *
   * This will set up the OrbitClient with the appropriate configuration.
   *
   * @param $configuration
   *   Optional configuration object can be passed to replace OAuth tokens.
   */
  public function __construct($configuration = NULL) {
    try {
      if (isset($configuration) || !isset($_SESSION['bynder_orbit_configuration'])) {
        if (!isset($configuration)) {
          $configuration = new Configuration(
            self::DEFAULT_ORBIT_API_URL,
            variable_get('bynder_orbit_oauth_client_id'),
            variable_get('bynder_orbit_oauth_client_secret'),
            url('orbit-oauth', ['absolute' => TRUE])
          );
          $tokens = $this->hasAccessToken();
          if (isset($tokens['oauth_access_token'])) {
            $oauthTokenObj = unserialize($tokens['oauth_access_token']);
            $configuration->setAccessToken($oauthTokenObj);
            $configuration->setRefreshToken($oauthTokenObj->getRefreshToken());
            $configuration->setRefreshCallbackFunction('BynderOrbitMediaApi::refreshToken');
            $_SESSION['bynder_orbit_configuration'] = serialize($configuration);
          }
        }
        $this->orbitClient = OrbitClient::create($configuration);
      }
      else {
        $configuration = unserialize($_SESSION['bynder_orbit_configuration']);
        $this->orbitClient = OrbitClient::create($configuration);
      }
    }
    catch (RequestException $e) {
      watchdog('bynder_orbit', $e->getMessage());
    }
  }

  /**
   * Factory function creating a new BynderOrbitMediaApi instance.
   */
  public static function createOrbitClient($configuration = NULL) {
    return new BynderOrbitMediaApi($configuration);
  }

  /**
   * Check if there is an OAuth token or code verifier in session.
   *
   * @return array|bool
   *   If it doesn't return false it means we either have tokens or we're in
   *   the authentication flow.
   */
  public function hasAccessToken() {
    $tokens = isset($_SESSION['bynder_orbit_data']) ? $_SESSION['bynder_orbit_data'] : [];
    // Required tokens need to be stored in the session.
    if (!isset($tokens['oauth_access_token']) && !isset($tokens['code_verifier'])) {
      return FALSE;
    }
    return $tokens;
  }

  /**
   * Refresh the OAuth token by overriding the previous one.
   *
   * Sets the new token in the session variables, which will be used when making
   * API requests.
   *
   * @param $accessToken
   *    Access token to override the old one.
   */
  public static function refreshToken($accessToken) {
    unset($_SESSION['bynder_orbit_configuration']);
    $_SESSION['bynder_orbit_data']['oauth_access_token'] = serialize($accessToken);
  }

  /**
   * Retrieves the Orbit Client asset service.
   *
   * The asset service is used for every Orbit API asset related operation.
   */
  public function getAssetService() {
    return $this->orbitClient->getAssetService();
  }

  /**
   * Checks if the module has valid settings configured.
   *
   * @return bool
   *    Returns true if both client ID and client Secret are set, false
   *    otherwise.
   */
  public function hasSettings() {
    $settings = [
      'clientId' => variable_get('bynder_orbit_oauth_client_id', FALSE),
      'cliendSecret' => variable_get('bynder_orbit_oauth_client_secret', FALSE),
    ];

    foreach ($settings as $setting) {
      if (!$setting) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Starts the OAuth authentication process.
   *
   * Calling this function will start the OAuth authorization process, which
   * opens the $authentication URL in a new tab where the user can
   * login to Orbit.
   */
  public function initiateOauthTokenRetrieval() {
    try {
      unset($_SESSION['bynder_orbit_configuration']);
      unset($_SESSION['bynder_orbit_data']);
      $this->createOrbitClient();
      $authenticationUrl = $this->orbitClient->getAuthorizationUrl();
      $codeVerifier = $this->orbitClient->getRequestHandler()
        ->getOauthCodeVerifier();

      $_SESSION['bynder_orbit_data'] = [
        'code_verifier' => $codeVerifier,
      ];
      header('Location: ' . $authenticationUrl);
    }
    catch (Exception $e) {
      watchdog('bynder_orbit', $e->getMessage());
      drupal_set_message(t('An error occurred while trying to login'), 'error');
      return FALSE;
    }

  }

  /**
   * Finishes the OAuth authorization process.
   *
   * This should be used as a callback when the user finishes login in to Orbit,
   * getting the final OAuth access tokens and setting it in session.
   *
   * @param $code
   *    Retrieved from the user login callback.
   *
   * @return bool|array
   *    If successful this call returns true, else an array with the error
   *    generated during execution.
   */
  public function finishOauthTokenRetrieval($code) {
    $tokens = $this->hasAccessToken();
    if ($tokens && isset($tokens['code_verifier'])) {
      $this->orbitClient->getRequestHandler()
        ->setOauthCodeVerifier($tokens['code_verifier']);
      try {
        $accessToken = $this->orbitClient->getAccessToken($code);
        $_SESSION['bynder_orbit_data']['oauth_access_token'] = serialize($accessToken);
        unset($_SESSION['bynder_orbit_data']['code_verifier']);
      }
      catch (Exception $e) {
        return $this->handleRequestError($e);
      }
    }
    else {
      $this->resetOauthTokens();
      drupal_set_message(t('Could not finish login, please try again'), 'error');
    }

    return TRUE;
  }

  /**
   * Helper function to handle API exceptions and return useful information.
   *
   * @param $exception
   *    An exception thrown from some point in the code.
   *
   * @return array
   *    Returns an error message inside an array for easier access at other
   *    points in the code.
   */
  private function handleRequestError($exception) {
    watchdog('bynder_orbit', $exception->getMessage());
    switch ($exception->getCode()) {
      case 401:
        $error = ['error' => t("The credentials used are not valid. Check if the access token and client ID/client secret are valid.")];
        $this->resetOauthTokens();
        break;

      default:
        $error = ['error' => t("There was an unexpected error when trying to reach the Orbit API.")];
        break;


    }
    if(!user_is_anonymous()) {
      drupal_set_message($error['error'], 'error', FALSE);
    }
    return $error;
  }

  /**
   * Removes OAuth tokens from session variables.
   */
  public function resetOauthTokens() {
    unset($_SESSION['bynder_orbit_data']);
    unset($_SESSION['bynder_orbit_configuration']);
  }

  /**
   * Returns a list of assets from Orbit.
   *
   * @param $query
   *    Query object will allow filtering and sorting the request.
   *
   * @return array
   *    The results will be returned in an array with 'data' & 'paging' values.
   *    If the request fails we will have an 'error' value in the array.
   */
  public function getAssets($query) {
    return $this->orbitApiRequest('assets', 'getAssets', $query);
  }

  /**
   * Returns a single asset from Orbit.
   *
   * @param $query
   *    Query object with the Asset ID
   *
   * @return array
   *    Result will be an array with all the properties of the Asset requested.
   *    If the request fails we will have an 'error' value in the array.
   */
  public function getAssetById($query) {
    return $this->orbitApiRequest('assets', 'getAssetById', $query);
  }

  /**
   * Returns the permanent preview url for a specific asset.
   *
   * @param $query
   *    Query object with the Asset ID, width and height.
   *
   * @return array
   *    The permanent preview URL.
   */
  public function getThumbnailUrl($query) {
      return $this->orbitApiRequest('assets', 'getPermanentPreviewURL', $query);
  }

  /**
   * Helper API request function, calling Orbit API functions.
   *
   * @param $serviceName
   *    Api service to call.
   * @param $method
   *    Which service method to call.
   * @param $query
   *    The query parameters.
   *
   * @return array
   *    Either an array with the values requested or an error message.
   */
  public function orbitApiRequest($serviceName, $method, $query) {
    try {
      switch ($serviceName) {
        case 'assets':
          $request = $this->orbitClient->getAssetService()->$method($query);
          return $request->wait();
      }
    }
    catch (Exception $e) {
      return $this->handleRequestError($e);
    }
  }


}
