<?php

/**
 * @file
 * Manages the Bynder media browser functionality.
 */

use Orbit\Api\Queries\AssetListQuery;
use Orbit\Api\Queries\AssetViewQuery;

const BYNDER_ORBIT_SEARCH_PAGE_SIZE = 10;

/**
 * Create a form to add files from Orbit.
 *
 * Menu callback for "media/add/orbit".
 */
function bynder_orbit_media_browser($form, &$form_state = []) {
  $orbit_api = BynderOrbitMediaApi::createOrbitClient();
  $tokens = $orbit_api->hasAccessToken();
  if (!$orbit_api->hasSettings()) {
    $form['message'] = [
      '#markup' => t("You haven't set up the 
            <a href='@admin_url' class='oauth-link' target='_blank'>
                Bynder Orbit settings</a> 
        properly, please do so before importing assets.",
        [
          '@admin_url' => url(
            'admin/config/media/orbit', ['absolute' => TRUE]),
        ]),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];
  }
  elseif (!$tokens || isset($tokens['code_verifier'])) {
    return bynder_orbit_media_browser_get_login();
  }
  else {

    $bynder_orbit_media_browser = bynder_orbit_generate_search($form_state);

    if (isset($bynder_orbit_media_browser['error']) && !$tokens) {
      return bynder_orbit_media_browser_get_login();
    }
    else {
      unset($bynder_orbit_media_browser['error']);
      $form['bynder_orbit_media_browser'] = $bynder_orbit_media_browser;
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
    ];

    $form['#attributes'] = ['class' => ['bynder-orbit-search-form']];

    $form['#attached']['library'][] = ['bynder_orbit', 'bynder_orbit_media'];
    $form['#attached']['css'][] = [
      'type' => 'external',
      'data' => '//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css',
    ];
  }
  return $form;
}

/**
 * Helper function to get a login link starting the Orbit OAuth flow.
 *
 * @return array
 *    The form markup with the login link.
 */
function bynder_orbit_media_browser_get_login() {
  $form['message'] = [
    '#markup' => t("You need to <a href='@login_url' class='oauth-link'
     target='_blank'>log in to Orbit</a> before importing assets.",
      [
        '@login_url' => url('orbit-oauth',
          ['absolute' => TRUE]),
      ]),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  ];
  $form['reload'] = [
    '#type' => 'button',
    '#value' => 'Reload after submit',
    '#attached' => ['library' => ['orbit/oauth']],
    '#attributes' => ['class' => ['oauth-reload', 'visually-hidden']],
  ];
  return $form;
}

/**
 * Page callback for Orbit OAuth login flow.
 *
 * @return array|bool
 *   If the OAuth flow is successful it returns page markup code to close the
 *   open tab, else returns TRUE/FALSE when polling if the login was successful.
 */
function bynder_orbit_oauth_login() {
  if (isset($_GET['state']) && isset($_GET['code'])) {
    $finish_token_retrieval = BynderOrbitMediaApi::createOrbitClient()
      ->finishOauthTokenRetrieval($_GET['code']);
    if (isset($finish_token_retrieval['error'])) {
      $_SESSION['bynder_login_error'] = $finish_token_retrieval['error'];
    }
    return [
      '#markup' => '<script>window.close()</script>',
      '#allowed_tags' => ['script'],
    ];
  }
  elseif (isset($_GET['oauth_check_login'])) {
    $access_token = BynderOrbitMediaApi::createOrbitClient()->hasAccessToken();
    if ($access_token && isset($access_token['oauth_access_token'])) {
      drupal_json_output(TRUE);
    }
    elseif (isset($_SESSION['bynder_login_error'])) {
      unset($_SESSION['bynder_login_error']);
      drupal_set_message(t('There is something wrong with your Bynder configuration, please confirm the details 
                in @settings and try again',
        ['@settings' => url('admin/config/media/orbit', ['absolute' => TRUE])]), 'error', FALSE);
      drupal_json_output(TRUE);
    }
    else {
      drupal_json_output(FALSE);
    }
  }
  else {
    try {
      BynderOrbitMediaApi::createOrbitClient()->initiateOauthTokenRetrieval();
    }
    catch (Exception $e) {
      $_SESSION['bynder_login_error'] = TRUE;
      return [
        '#markup' => '<script>window.close()</script>',
        '#allowed_tags' => ['script'],
      ];
    }
  }
}

/**
 * Generates search page markup.
 *
 * @param $form_state
 *    The existing form_state.
 *
 * @return array
 *    The search page markup, together with the search results.
 *
 * @throws Exception
 */
function bynder_orbit_generate_search($form_state) {
  $search = bynder_orbit_get_media_form_filter_value($form_state, 'search_field', '');
  // Create search section.
  $data['search'] = [
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#attributes' => [
      'class' => [
        'bynder-search-form',
      ],
    ],
  ];
  $data['search']['search_field'] = [
    '#title' => t('Search'),
    '#type' => 'textfield',
    '#default_value' => isset($search) ? $search : '',
    '#description' => "<p class='alert alert-warning' style='display:none'><span class='text'></span></p>",
  ];

  $sort_order = bynder_orbit_get_media_form_filter_value($form_state, 'sort_order', '-created_at');

  $data['search']['sort_order'] = [
    '#title' => t('Sort By'),
    '#type' => 'select',
    '#options' => [
      'name' => t('Name (A-Z)'),
      '-name' => t('Name (Z-A)'),
      '-created_at' => t('Date added (Newest - Oldest)'),
      'created_at' => t('Date added (Oldest - Newest)'),
    ],
    '#default_value' => $sort_order,
  ];

  $extension_filter = bynder_orbit_get_media_form_filter_value($form_state, 'filter_extension', '');

  $data['search']['filter_extension'] = [
    '#title' => t('Extension'),
    '#type' => 'select',
    '#multiple' => TRUE,
    '#options' => [
      'jpg' => t('JPG'),
      'jpeg' => t('JPEG'),
      'png' => t('PNG'),
      'svg' => t('SVG'),
    ],
    '#default_value' => isset($extension_filter) ? array_values(explode(',', $extension_filter)) : '',
  ];

  $data['search']['button'] = [
    '#type' => 'button',
    '#value' => t('Search'),
  ];

  $data['results'] = [
    '#type' => 'container',
    '#attributes' => [
      'id' => ['results_list'],
      'class' => ['grid'],
    ],
  ];
  $results = bynder_orbit_search_results($search, $sort_order, $extension_filter, BYNDER_SEARCH_PAGE_SIZE);

  if (isset($results['data']) && !isset($results['error'])) {
    // Create the results section.
    $data['results'][] = bynder_orbit_generate_results_grid($results['data']);
    if (isset($results['totalNrOfResults'])) {
      $data['pager_container'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['.js-form-type-entity-browser-pager'],
        ],
      ];
      $data['pager_container']['total_results'] = [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#attributes' => [
          'class' => 'total_results',
        ],
        '#value' => t('Total Results: @numResults',
          ['@numResults' => $results['totalNrOfResults']]),
      ];
      $data['pager_container']['pager'] = bynder_orbit_generate_pager(
        $results['totalNrOfResults'], ['search_field' => $search]
      );
    }
  }
  else {
    if (isset($results['error'])) {
      drupal_set_message(
        t("There was an error generating the search results: @error",
          ['@error' => $results['error']]), 'error', FALSE);
      $data['error'] = $results['error'];
    }
    $data['results'][] = bynder_orbit_no_results_found();
  }

  return $data;
}

/**
 * Fetches the Orbit search results according to the passed parameters.
 *
 * @param string $search
 *    Search keyword.
 * @param string $sort_order
 *    Sorting parameter.
 * @param string $filters
 *    Search filters.
 * @param int $nr_of_results
 *    Number of results to return, defaults to 5.
 *
 * @return array
 *    The list of assets resulting from the search, with a count value.
 */
function bynder_orbit_search_results($search, $sort_order, $filters = '', $nr_of_results = 5) {
  $start_index = 0;
  $query = AssetListQuery::create();

  if (isset($search) && strlen($search) > 0) {
    $query->setQuery(urlencode($search));
  }

  if (isset($sort_order)) {
    $query->setSort($sort_order);
  }

  if (!empty($filters)) {
    $extension_filter = $filters;
  }

  if (isset($_POST['search_field']) && $_POST['search_field'] != $search
    || isset($_POST['filter_extension'])
    && $_POST['filter_extension'] != $filters) {
    // If we're doing a search, make sure we get the first page.
    $_GET['page'] = 0;
  }

  if (isset($_GET['page'])) {
    $current_page = (int) check_plain($_GET['page']);
    $start_index = $current_page * $nr_of_results;
  }
  $query
    ->setLimit($nr_of_results)
    ->setFrom($start_index)
    // Only supporting images at the moment.
    ->setExtension(
      !empty($extension_filter) ? $extension_filter : 'jpeg,jpg,png,svg');

  $result = BynderOrbitMediaApi::createOrbitClient()->getAssets($query);
  if (!empty($result) && isset($result['paging']['count'])) {
    $result['totalNrOfResults'] = $result['paging']['count'];
  }
  return $result;
}

/**
 * Generates the assets grid from the search results.
 *
 * @param $results
 *    The search results.
 *
 * @return array
 *    The results grid in Drupal form markdown format.
 */
function bynder_orbit_generate_results_grid($results) {
  $output = [];
  foreach ($results as $media_item) {
    if (isset($media_item['current_version']['previews']['images']['thumbnail'])) {
      $output['thumbnail-' . $media_item['asset_id']] = [
        '#type' => 'container',
        '#attributes' => [
          'id' => $media_item['asset_id'],
          'class' => ['grid-item'],
        ],
      ];
      $output['thumbnail-' . $media_item['asset_id']]['check_' . $media_item['asset_id']] = [
        '#type' => 'checkbox',
        '#parents' => ['selected_assets', $media_item['asset_id']],
        '#attributes' => ['class' => ['item-selector']],
      ];
      $output['thumbnail-' . $media_item['asset_id']]['image'] = [
        '#name' => $media_item['name'],
        '#type' => 'markup',
        '#markup' => '<img src="' . $media_item['current_version']['previews']['images']['thumbnail']['url'] . '" alt="' . $media_item['name'] . '"/>
                                <div class="more-info">
                                  <h4 class="name">' . $media_item['name'] . '</h4>
                                </div>',
      ];
    }
  }
  return $output;
}

/**
 * The Orbit media browser form submit handler.
 *
 * Saves the selected assets information to Drupal.
 *
 * @param $form
 *    The search form.
 * @param $form_state
 *    Search form current state.
 */
function bynder_orbit_media_browser_submit($form, &$form_state) {
  $selected_assets = isset($form_state['values']['selected_assets']) ? $form_state['values']['selected_assets'] : NULL;
  if ($selected_assets) {
    $fids = [];
    foreach ($selected_assets as $asset_id => $selected) {
      if ($selected) {
        $result = bynder_orbit_save_image($asset_id, $form, $form_state);
        if (isset($result['success'])) {
          $fids[] = $result['fid'];
        }
        else {
          drupal_set_message(
            t("@error_message", ["error_message" => $result['message']]),
            'error');
        }
      }
    }
    // Save the files.
    if (!empty($fids)) {
      if (current_path() != 'admin/content/media/add/orbit') {
        drupal_goto('/media/browser', [
          'query' => [
            'render' => 'media-popup',
            'fid' => $fids,
          ],
        ]);
      }
      else {
        drupal_set_message(
          t("Successfully added the selected assets."), 'status');
        drupal_goto('admin/content/file');
      }
    }
    else {
      drupal_set_message(
        t("Couldn't add any assets, please check the error logs for
        more information."), 'warning');
    }
  }
}

/**
 * Helper function to store Orbit asset information in the database.
 *
 * @param $id
 *    Orbit asset ID
 * @param $form
 *    The media browser form
 * @param $form_state
 *    Media browser form state.
 *
 * @return array|bool
 *    Either an array with the result of adding the image to the system,
 *    or FALSE if there was an error retrieving asset information.
 */
function bynder_orbit_save_image($id, $form, &$form_state) {
  $selected_media = BynderOrbitMediaApi::createOrbitClient()->getAssetById(
    AssetViewQuery::create($id)
  );

  if (isset($selected_media['error'])) {
    return FALSE;
  }

  $id_hash = $selected_media['name'];

  $uri = file_stream_wrapper_uri_normalize("orbit://f/$id/i/$id_hash");

  $files = file_load_multiple([], ['uri' => $uri]);
  $file = !empty($files) ? reset($files) : file_uri_to_object($uri);

  if ($file === FALSE) {
    return [
      'success' => 0,
      'message' => 'An error occurred and no file was added to your Library.',
    ];
  }
  elseif (!isset($file->fid)) {
    $file->filemime = 'image/bynder_orbit';
    $file->filesize = 0;
    $file->filename = $selected_media['name'];

    $file = file_save($file);
    field_attach_submit('media', $file, $form, $form_state);
    $file->file = [];
    $file->file[LANGUAGE_NONE] = [];
    $file->file[LANGUAGE_NONE][0] = (array) $file + ['display' => TRUE];
    $file->is_new = TRUE;
    field_attach_insert('media', $file);

    bynder_orbit_create_media_entry($selected_media, $file->fid);

    return [
      'success' => 1,
      'message' => 'Successfully added media to Library',
      'fid' => $file->fid,
    ];
  }
  else {
    return [
      'success' => 1,
      'message' => 'Already exists in Library',
      'fid' => $file->fid,
    ];
  }
}

/**
 * Creates an Orbit media entry in the database.
 *
 * @param $selected_media
 *    The Orbit asset array.
 * @param $fid
 *    Drupal file ID.
 */
function bynder_orbit_create_media_entry($selected_media, $fid) {
  // Save Bynder asset entry.
  $bynder_id = $selected_media['asset_id'];
  $name = $selected_media['name'];
  $description = isset($selected_media['description'])
    ? $selected_media['description'] : $selected_media['name'];
  try {
    db_insert('bynder_orbit_media_entity')
      ->fields([
        'bynder_orbit_id' => $bynder_id,
        'name' => $name,
        'description' => $description,
        'previews' => serialize($selected_media['current_version']['previews']['images']),
        'fid' => $fid,
      ])
      ->execute();

  }
  catch (Exception $e) {
    // Handle exception.
    watchdog('bynder', $e->getMessage());
  }
}

/**
 * Returns a comma separated list of form values to use in the search.
 *
 * @param $form_state
 *    The search form state.
 * @param $field_name
 *    The search form field.
 * @param $default_value
 *    Default value to use for the parameter.
 *
 * @return string
 *    Comma separated list of values to use in the search/sorting field.
 */
function bynder_orbit_get_media_form_filter_value($form_state, $field_name, $default_value) {

  if (isset($form_state['values'][$field_name])) {
    $field_value = is_array($form_state['values'][$field_name]) ? implode(',', $form_state['values'][$field_name]) : $form_state['values'][$field_name];
    return check_plain($field_value);
  }
  elseif (isset($_GET[$field_name])) {
    return check_plain($_GET[$field_name]);
  }

  return $default_value;
}

/**
 * Generate paging element for the media browser form.
 *
 * @param $num_results
 *    The total number of search results.
 * @param $parameters
 *    The search parameters to store during pagination.
 *
 * @throws Exception
 *
 * @return array
 *    Pager element markup.
 */
function bynder_orbit_generate_pager($num_results, $parameters = []) {
  if (isset($_POST['search_field'])) {
    $_GET['search_field'] = check_plain($_POST['search_field']);
  }
  if (isset($_POST['sort_order'])) {
    $_GET['sort_order'] = check_plain($_POST['sort_order']);
  }
  if (isset($_POST['filter_extension'])) {
    $_GET['filter_extension'] = check_plain(implode(',', $_POST['filter_extension']));
  }
  pager_default_initialize($num_results, BYNDER_SEARCH_PAGE_SIZE);
  return [
    '#markup' => theme('pager', [
      'parameters' => $parameters,
      'element' => 0,
    ]),
  ];
}

/**
 * Returns a simple "No results found" HTML section.
 *
 * @retun array
 *    HTML markup of no results section.
 */
function bynder_orbit_no_results_found() {
  return [
    '#name' => 'orbit-no-results-section',
    '#attributes' => ['class' => ['orbit-no-results-section']],
    '#type' => 'markup',
    '#markup' => '<div><h3>' . t('Could not retrieve search results.') . '</h3></div>',
  ];
}
