<?php

/**
 * @file
 * Media browser plugin implementation.
 */

/**
 * Browser plugin for Orbit files.
 */
class BynderOrbitMediaBrowser extends MediaBrowserPlugin {

  /**
   * Implements MediaBrowserPluginInterface::access().
   */
  public function access($account = NULL) {
    return user_access('administer media bynder orbit', $account);
  }

  /**
   * Implements MediaBrowserPlugin::view().
   */
  public function view() {
    return ['form' => drupal_get_form('bynder_orbit_media_browser', $this->params)];
  }

}
