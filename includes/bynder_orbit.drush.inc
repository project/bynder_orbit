<?php

/**
 * @file
 * Drush related file, setting cache id for the module.
 */

/**
 * Implements hook_drush_cache_clear().
 */
function bynder_orbit_drush_cache_clear(&$types) {
  $types['bynder_orbit'] = '_bynder_orbit_cache_clear';
}

/**
 * Utility function that clears all the entries in our cache bin.
 */
function _bynder_orbit_cache_clear() {
  cache_clear_all('*', 'cache_bynder_orbit', TRUE);
}
