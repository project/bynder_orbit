<?php

/**
 * @file
 * Formatters for Media: Bynder.
 */

/**
 * Implements hook_file_formatter_info_alter().
 */
function bynder_orbit_file_formatter_info_alter(&$info) {
  $info['file_bynder_orbit_image'] = [
    'label' => t('Bynder Orbit Preview Image'),
    'file types' => ['image'],
    'default settings' => ['image_style' => ''],
    'view callback' => 'bynder_orbit_file_formatter_image_view',
    'settings callback' => 'bynder_orbit_file_formatter_image_settings',
    'mime types' => [
      'image/bynder_orbit',
    ],
  ];

  return $info;
}

/**
 * Format image view.
 *
 * Implements hook_file_formatter_FORMATTER_view().
 */
function bynder_orbit_file_formatter_image_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);
  if ($scheme == 'orbit') {
    $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
    $image_style = $display['settings']['image_style'];
    $element = [
      '#theme' => 'image',
      '#path' => $wrapper->getWebImagePath($file->filename, $image_style),
      '#style_name' => $image_style,
      '#alt' => isset($file->override['attributes']['alt']) ? $file->override['attributes']['alt'] : $file->filename,
    ];
    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function bynder_orbit_file_formatter_image_settings($form, &$form_state, $settings) {
  $element = [];
  $element['image_style'] = [
    '#title' => t('Image style'),
    '#type' => 'select',
    '#options' => image_style_options(FALSE),
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
  ];
  return $element;
}
