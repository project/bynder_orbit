<?php

/**
 * @file
 * File holding admin configuration form and.
 */

/**
 * Form callback for `/admin/config/media/orbit`.
 */
function bynder_orbit_form($form, &$form_state) {
  $form['bynder_orbit_settings'] = [
    '#type' => 'fieldset',
    '#title' => t('Bynder Orbit API settings'),
    '#description' => t('You will first need to generate your OAuth credentials, 
      see the steps to do so on the project documentation page.'),
    '#collapsible' => TRUE,
  ];

  $form['bynder_orbit_settings']['bynder_orbit_oauth_client_id'] = [
    '#type' => 'textfield',
    '#title' => t('Orbit OAuth client ID'),
    '#default_value' => variable_get('bynder_orbit_oauth_client_id', ''),
    '#maxlength' => 500,
    '#description' => t('Orbit OAuth client ID.'),
    '#required' => TRUE,
  ];

  $form['bynder_orbit_settings']['bynder_orbit_oauth_client_secret'] = [
    '#type' => 'textfield',
    '#title' => t('Orbit OAuth client secret'),
    '#default_value' => variable_get('bynder_orbit_oauth_client_secret', ''),
    '#maxlength' => 500,
    '#description' => t('Orbit OAuth client secret.'),
    '#required' => TRUE,
  ];

  $form['#submit'][] = 'bynder_orbit_form_submit';

  return system_settings_form($form);
}

/**
 * Admin form submit, force reloading previews the first time we save the form.
 */
function bynder_orbit_form_submit($form, &$form_state) {
  bynder_orbit_batch_reload_previews($form, $form_state);
  cache_clear_all('*', 'cache_bynder_orbit', TRUE);
}

/**
 * Batch callback to fetch preview information.
 */
function bynder_orbit_batch_reload_previews($form, &$form_state) {
  $batch = [
    'operations' => [
      ['bynder_orbit_batch_reload_previews_process', []],
    ],
    'finished' => 'bynder_orbit_batch_reload_previews_finished',
    'title' => t('Batch to reload Bynder previews information'),
    'init_message' => t('Bynder previews batch is starting...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Fix has encountered an error.'),
  ];
  batch_set($batch);
}

/**
 * Processes the preview list and sets new image styles for each one.
 */
function bynder_orbit_batch_reload_previews_process(&$context) {
  $context['message'] = t("Updating previews data.");
  $previews = [
    'small',
    'thumbnail',
    'overview',
    'detail'
  ];
  variable_set('bynder_orbit_previews', serialize($previews));
  foreach ($previews as $preview) {
    try {
      if (!image_style_load('bynder_orbit_' . $preview)) {
        $context['message'] = t("Creating image style for preview: @preview", ['@preview' => $preview]);
        image_style_save(
          [
            'name' => 'bynder_orbit_' . $preview,
            'label' => t('Bynder Orbit preview: @preview',
                                                  ['@preview' => $preview]),
          ]
        );

      }
    }
    catch (Exception $e) {
      watchdog('bynder', $e->getMessage());
      $context['finished'] = 1;
      $context['results']['error'] = t("There was an error while saving new image styles.");
    }
  }
}

/**
 * Batch finished callback.
 */
function bynder_orbit_batch_reload_previews_finished($success, $results, $operations) {
  if ($success) {
    if (isset($results['error'])) {
      drupal_set_message($results['error'], 'error');
    }
    else {
      drupal_set_message(t('Finished setting up image styles!'));
    }
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', [
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE),
    ]);
    drupal_set_message($message, 'error');
  }
}
