<?php

/**
 * @file
 * Create a Bynder Stream Wrapper class for the Media/Resource module.
 */

/**
 * Class BynderOrbitMediaStreamWrapper.
 *
 * Create a Bynder Stream Wrapper class for the Media/Resource module.
 */
class BynderOrbitMediaStreamWrapper extends MediaReadOnlyStreamWrapper {

  /**
   * Function getWebimagePath().
   *
   * @param $filename
   *    Asset filename.
   * @param $image_style
   *    Image style.
   *
   * @return string
   *   The path to the image.
   *
   * @throws Exception
   */
  public function getWebimagePath($filename, $image_style) {
    $params = $this->parameters;
    return @bynder_orbit_embed_url($filename, $params['f'], $image_style);
  }

}
