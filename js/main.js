(function ($) {
    'use strict';
    var timeoutDuration = 3000;
    var oauthRetries = 20;
    $(document).on('click', '.oauth-link', function () {
        var url = $('.oauth-link').attr('href');
        (function loop(i) {
            setTimeout(function () {
                $.ajax({
                    url: url,
                    data: {'oauth_check_login': true}
                }).done(function (data) {
                    if (data === true) {

                        $('.oauth-reload').click();
                        return;
                    }
                    if (--i) loop(i);
                });
            }, timeoutDuration)
        })(oauthRetries);

    });
})(jQuery);
