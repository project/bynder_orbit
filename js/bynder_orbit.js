(function ($) {
    Drupal.behaviors.bynderMedia = {
        attach: function () {
            'use strict';

            var spinner;

            var showSpinner = function (element) {
                spinner = new Spinner({
                    lines: 13,
                    length: 20,
                    width: 10,
                    radius: 30,
                    corners: 1,
                    rotate: 0,
                    direction: 1,
                    color: '#000',
                    speed: 1,
                    trail: 60,
                    shadow: true,
                    hwaccel: false,
                    className: 'spinner',
                    zIndex: 2e9,
                    top: '50%',
                    left: '50%'
                }).spin(element);
            };

            $(document).ready(function () {
                var $grid = $('.grid');
                $grid.prepend('<div class="grid-sizer"></div><div class="gutter-sizer"></div>').once();
                // layout Masonry after each image loads
                $grid.imagesLoaded().progress(function () {
                    $grid.masonry({
                        columnWidth: '.grid-sizer',
                        gutter: '.gutter-sizer',
                        itemSelector: '.grid-item'
                    });
                });

                $('.grid-item').once('bynder-bind-click-event').click(function () {
                    var $input = $(this).find('.item-selector');
                    var itemSelector = $(this);


                    $input.prop('checked', function (i, val) {
                        var isChecked = !val;
                        if (isChecked) {
                            itemSelector.addClass('checked');
                        }
                        else {
                            itemSelector.removeClass('checked');
                        }
                        return isChecked;
                    });

                });

                $('#bynder-orbit-media-browser').submit(function (e) {
                    $('#bynder-orbit-media-browser').prepend('<div class="overlay-throbber"><div class="throbber-spinner"></div></div></div>');
                    var elem = $('.throbber-spinner');
                    $(elem).addClass('spinning');
                    showSpinner($(elem)[0]);
                });
            });
        }
    }

})(jQuery);
