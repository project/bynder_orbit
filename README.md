Bynder Orbit module
====================

INTRODUCTION
------------

The <a href="https://www.bynder.com/en/orbit/">Bynder Orbit</a> integration module provides seamless access to Orbit assets in your Drupal website. This integration will allow the authorised users to:
<ul>
  <li>Import Assets from Orbit</li>
  <li>Display image styles imported from Orbit</li>
</ul>

REQUIREMENTS
------------

This module requires the following modules:
<ul>
	<li><a href="https://www.drupal.org/project/media">Media</a> - The Media Browser is an essential part of the module.</li>
	<li><a href="https://www.drupal.org/project/jquery_update">jQuery Update</a> - This module requires a recent version (1.7+)&nbsp;of jQuery in order to work.</li>
	<li><a href="https://www.drupal.org/project/composer_manager">Composer Manager</a> - We use composer to manage library dependencies.</li>
</ul>

<h3 id="project-dependencies">Other dependencies</h3>
In order to start using this module you'll first need a Bynder Orbit account and to generate your OAuth2 credentials. Have a look in the <a href="https://www.drupal.org/docs/7/modules/bynder-orbit/before-you-get-started">Before you get started</a> section of the  documentation for more details on how to get started.

INSTALLATION
------------
Enabling the module is just a part of what's necessary for the Bynder Orbit Media browser to work. Please refer to the module's <a href="https://www.drupal.org/docs/7/modules/bynder-orbit">documentation guide</a> in order to properly configure it.

 
 CONFIGURATION
 ------------- 
 Follow the steps on the <a href="https://www.drupal.org/docs/7/modules/bynder-orbit/installation-and-configuration">installation and configuration page</a> of the documentation.